//Creating variables to store the two different players ('X' and 'O')
//Also creating variable to store results of each player's selection ['X' selected cell id 1; 'O' selected cell id 2; etc.]

let currentPlayer = 'X';
let nextPlayer = 'O';
let playerXSelections = [];
let playerOSelections = [];
//Creating an array made up of arrays which contain the winning combinations
const winningCombinations = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 5, 9],
    [3, 5, 7],
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9]
];

//in HTML file, we gave id numbers 1-9 to each individual cell in the table
//this will allow us to detect and identify when a cell is clicked and which cell was clicked

//Now we are creating an Event Listener function
//Code below does the following: creates function called handleClick()
//handleClick() will log the ID of the cell that was clicked
//handleClick() gets an array of all the cells by using document.querySelectorAll()
//It will iterate through all those cells to add an Event Listener
//---------Next Step----
//Now we want to print currentPlayer's move ('X', then 'O' for next player) on the board
//After we print the move on the board, we are going to switch to the next player
//So we remove the console.log() and add .innerHTML -->Went wrong here somewhere.
//All of this must be included IN the function


const handleClick = function(event) {
        const cell = event.target;
        cell.innerHTML = currentPlayer;
        if (currentPlayer === 'X') {
            playerSelections = playerXSelections;
            nextPlayer = 'O';
        } else {
            playerSelections = playerOSelections;
            nextPlayer = 'X';
        }

        playerSelections.push(Number(cell.id));

        if (checkWinner(playerSelections)) {
            alert('Player ' + currentPlayer + 'wins the game!');
            resetGame();
        }
        if (checkDraw()) {
            alert('There has been a draw!');
            resetGame();
        }

        //Now we swap players
        currentPlayer = nextPlayer;
    }
    //How do we check for a winning combination?  We have the array with all the winning combinations
    //Not sure if this function is written properly
    //direction states we could have used the .includes array method for line 58

function checkWinner() {
    for (i = 0; i < winningCombinations.length; i++) {
        let winner = winningCombinations[i];
        matches = 0;
        for (let j = 0; j < winner; j++) {
            if (currentPlayer || nextPlayer === winner) {
                matches++;

            } else break
            if (matches === 3) {
                return true;
            } else {
                return false;
            }
        }
    }
}
//Now lets check for a Draw!  A game to feed to the cat!
function checkDraw() {
    return (playerOSelections.length + playerXSelections.length) >= cells.length;
}

//We need to be able to reset the game
//We need to clear player selections and remove X's and O'x from board
function resetGame() {
    playerXSelections = new Array();
    playerOSelections = new Array();
    for (let i = 0; i < cells.length; i++) {
        cells[i].innerHTML = '';
    }
}


const cells = document.querySelectorAll('td');
for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', handleClick)
};